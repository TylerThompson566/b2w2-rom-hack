#B2W2 Rom Hack
b2w2.nds - base file
hack.nds - hack file

##HOW-TO FOR ROM HACKING B2W2

###Unpackage .nds rom
- open thenewpoketext.exe
- enter the name of the rom (including the .nds file extension)

###Package folder into .nds file
- open thenewpoketext.exe
- enter the name of the folder (excluding teh 'tmp_')
- enter command 'mkrom [romname]'

###Unpackage files as narcs
- open nitro explorer 3
- select any file
- select extract, when extracting, add the .narc file extension
- open NDSeditor.exe
- drag the .narc file into the window
- double click the file
- check the root of the folder and click on the 'extract selected' option

###Repackage files into narcs
- open NDSeditor.exe
- tools -> make narc file
- select the folder to edit

###Edit Trainer Data and Pokemon
- unpackage a/0/9/1 as trdata
- unpackage a/0/9/2 as trpoke
- open BlackWhiteTrainerEditor.exe
- select the unpackaged folders as trdata and trpoke respectively
- make the desired edits
- repackage